<?php

Required::from("/api/db/Database.php");
Required::from("/api/db/IEntity.php");

abstract class EPrivelege {
    const DEFAULT = 0;
    const MODERATOR = 1;
    const ADMIN = 2;

    public static function toString(int $privelegeNumber) {
        switch($privelegeNumber) {
            case self::DEFAULT:
                return "DEFAULT";
                break;
            case self::MODERATOR:
                return "MODERATOR";
                break;
            case self::ADMIN:
                return "ADMIN";
                break;
            default:
                return "";
                break;
        }
    }
}

class User implements IEntity {
    const SECRET = "secret";
    const AllowedEditProperties = array("user_password");

    private int $user_id;
    private string $user_name;
    private string $user_password;
    private int $user_privelege;

    public function __construct($user_name, $user_password, $user_id = -1, $user_privelege = EPrivelege::DEFAULT) {
        $this->user_id = $user_id;
        $this->user_name = $user_name;
        $this->user_password = $user_password;
        $this->user_privelege = $user_privelege;
    }

    public function save(): bool {
        // If these values don't exist then don't save the User
        if($this->user_name === null) return false;
        if($this->user_password === null) return false;
        if($this->user_privelege === null) return false;

        // Check if user exists already
        $checkUserExistsQuery = <<<SQL
            SELECT * FROM users WHERE user_name="{$this->user_name}";
        SQL;

        $result = Database::GetConnection()->query($checkUserExistsQuery);
        if($result->num_rows !== 0) return false;

        $hashedPassword = password_hash($this->user_password, PASSWORD_DEFAULT);

        // Return whether the User actually successfully saved
        $userAdded = Database::GetConnection()->query(<<<SQL
            INSERT INTO users (user_name, user_password, user_privelege)
            VALUES ("{$this->user_name}", "{$hashedPassword}", {$this->user_privelege});
        SQL);

        return $userAdded;
    }

    public function exists(): bool {
        if($this->user_name === null) return false;
        if($this->user_password === null) return false;

        // Get user in Database with the matching user_name
        $getUserQuery = <<<SQL
            SELECT * FROM users WHERE user_name="{$this->user_name}"
        SQL;

        $result = Database::GetConnection()->query($getUserQuery);
        if($result->num_rows === 0) return false;

        $userInDatabase = $result->fetch_assoc();

        $this->user_id = $userInDatabase['user_id'];

        $passwordHashMatches = password_verify($this->user_password, $userInDatabase["user_password"]);

        return $passwordHashMatches;
    }

    public static function getById($id) {
        $getUserByIdQuery = <<<SQL
            SELECT * FROM users WHERE user_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($getUserByIdQuery);
        if(!$resultOfQuery) return null;

        $userDetails = $resultOfQuery->fetch_assoc();

        return new User($userDetails['user_name'], "", $id, $userDetails['user_privelege']);
    }

    public static function editById($id, $entityProperties): bool {
        // Entity properties will be an associative array with properties
        // that you want to change the entity with so we need to transform it
        // into an array of strings

        $destructeredEntityProperties = array_map(function($key, $value) {
            if(in_array($key, self::AllowedEditProperties)) {
                if($key === "user_password") {
                    // In this case this is the only thing allowed to be
                    // Edited but we need to actually manipulate the password
                    // specifically before it goes into the DB
                    $escapedPassword = Database::GetConnection()->escape_string(password_hash($value, PASSWORD_DEFAULT));
                    return "$key='$escapedPassword'";

                } else {
                    $escapedValue = Database::GetConnection()->escape_string($value);
                    return "$key={$escapedValue}";
                }
            }
        }, array_keys($entityProperties), $entityProperties);
        if(sizeof($destructeredEntityProperties) === 0) return false;

        $removeEmptyProperties = array_filter($destructeredEntityProperties, fn($property) => strlen($property) !== 0);

        $reducedProperties = implode(",", $removeEmptyProperties);

        $editUserByIdQuery = <<<SQL
            UPDATE users SET {$reducedProperties} WHERE user_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($editUserByIdQuery);
        if(!$resultOfQuery) return false;

        return true;
    }

    public static function deleteById($id): bool {
        $deleteUserByIdQuery = <<<SQL
            DELETE FROM users WHERE user_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($deleteUserByIdQuery);
        if(!$resultOfQuery) return false;

        return true;
    }

    /**
     * Creating JSON Web Tokens (JWTs) is not trivial so I looked up a guide
     * https://dev.to/robdwaller/how-to-create-a-json-web-token-using-php-3gml
     *
     * The code provided is used in this guys library which is under an MIT license:
     * https://github.com/RobDWaller/ReallySimpleJWT/blob/master/LICENSE
     *
     * What this will do is create a JSON Web Token from scratch,
     * JWTs are a way to pass the credentials of the user around to the server.
     * They allow for scalability since we don't have to store a session ID of
     * the user like you typically would with a user. This lets us have multiple
     * PHP servers being used in the backend should the amount of people going to your
     * site rise.
     *
     * Later on in the code there will be a function that will decode the token
     * and use that to determine if the user has permission to do certain actions.
     *
     * One thing that is not being done is there is no expiry since it is just
     * a pet project, but normally inside the JWT you'd include a datetime to
     * say "this token is not valid anymore" after a certain point.
     */
    public function generateToken(): string {
        // Create token header as a JSON string
        $header = json_encode(array('typ' => 'JWT', 'alg' => 'HS256'));

        // Create token payload as a JSON string
        $payload = json_encode(array("user_id" => $this->user_id));

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, self::SECRET, true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }

    public static function decodeToken($token) {
        if($token === null) return null;
        // Remove front of token if it exists
        $strippedToken = str_replace("Bearer ", "", $token);

        // Get the info in the form of header.payload.signature
        [$base64UrlHeader, $base64UrlPayload, $base64UrlSignature] = explode('.', $strippedToken);

        // The included signature should match the header and payload hashed right now
        // if it doesn't match then someone modified the data
        $includedSignature = base64_decode(str_replace(['-', '_', ''], ['+', '/', '='], $base64UrlSignature));
        $calculatedSignature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, self::SECRET, true);

        if(!hash_equals($includedSignature, $calculatedSignature)) {
            return null;
        }

        // We are going to put it back into a proper Base64 string
        $base64Payload = str_replace(['-', '_', ''], ['+', '/', '='], $base64UrlPayload);

        // We want to return this data to the server to know who the authenticated user is
        $payload = base64_decode($base64Payload);

        return json_decode($payload, true);
    }

    public function getUserName() {
        return $this->user_name;
    }

    public function getPrivelege() {
        return $this->user_privelege;
    }

    public function getId() {
        return $this->user_id;
    }

    public function isAdmin() {
        return $this->user_privelege === EPrivelege::ADMIN;
    }

}


?>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/api/db/Database.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/api/db/IEntity.php';

class Post implements IEntity {
    const AllowedEditProperties = array("post_title", "category_id");

    private int $post_id;
    private int $category_id;
    private int $user_id;
    private string $post_title;
    private $post_created;

    public function __construct($category_id, $user_id, $post_title, $post_id = -1, $post_created = null) {
        $this->post_created = $post_created;
        $this->post_id = $post_id;
        $this->category_id = $category_id;
        $this->user_id = $user_id;
        $this->post_title = $post_title;
    }

    public function save(): bool {
        if($this->category_id === null) return false;
        if($this->user_id === null) return false;
        if($this->post_title === null) return false;

        $createPostQuery = <<<SQL
            INSERT INTO posts (post_category_id, post_user_id, post_title)
            VALUES ({$this->category_id}, {$this->user_id}, "{$this->post_title}");
        SQL;

        $result = Database::GetConnection()->query($createPostQuery);
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function getById($id) {
        // Select from the Database
        $selectQuery = <<<SQL
            SELECT * FROM posts WHERE post_id={$id};
        SQL;

        $result = Database::GetConnection()->query($selectQuery);
        if(!$result) return null;

        $postDetails = $result->fetch_assoc();

        return new Post($postDetails['post_category_id'], $postDetails['post_user_id'], $postDetails['post_title'], $postDetails['post_id'], $postDetails['post_created']);
    }

    public static function editById($id, $entityProperties): bool {
        // Entity properties will be an associative array with properties
        // that you want to change the entity with so we need to transform it
        // into an array of strings

        $destructeredEntityProperties = array_map(function($key, $value) {
            if(in_array($key, self::AllowedEditProperties)) {
                $escapedValue = Database::GetConnection()->escape_string($value);
                return "$key={$escapedValue}";
            }
        }, array_keys($entityProperties), $entityProperties);
        if(sizeof($destructeredEntityProperties) === 0) return false;

        $removeEmptyProperties = array_filter($destructeredEntityProperties, fn($property) => strlen($property) !== 0);

        $reducedProperties = implode(",", $removeEmptyProperties);
        $editPostByIdQuery = <<<SQL
            UPDATE posts SET {$reducedProperties} WHERE post_id={$id};
        SQL;

        $result = Database::GetConnection()->query($editPostByIdQuery);
        if(!$result) return false;

        return true;
    }

    public static function deleteById($id): bool {
        $deletePostByIdQuery = <<<SQL
            DELETE FROM users WHERE user_id={$id};
        SQL;

        $result = Database::GetConnection()->query($deletePostByIdQuery);
        if(!$result) return false;

        return false;
    }

    public static function getAllPostsWithCategoryId($category_id) {
        $getAllPostsWithCategoryIdQuery = <<<SQL
            SELECT *
            FROM test.posts p
            INNER JOIN (SELECT user_id, user_name, user_privelege FROM test.users) u
            ON p.post_user_id=u.user_id
            WHERE post_category_id={$category_id};
        SQL;

        $result = Database::GetConnection()->query($getAllPostsWithCategoryIdQuery);
        if(!$result) return false;

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getId() {
        return $this->post_id;
    }

    public function getCategoryId() {
        return $this->category_id;
    }

    public function getTitle() {
        return $this->post_title;
    }

    public function getUserId() {
        return $this->user_id;
    }


}



?>
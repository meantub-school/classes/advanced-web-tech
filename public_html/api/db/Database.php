<?php
class Database {
    // I am only storing these values in the code like this because
    // if sent to you, you'd have to create your own .env file to
    // supply these values
    private static string $host = "mysql";
    private static string $user = "root";
    private static string $pass = "root";
    private static string $db = "test";
    private static int $port = 3306;

    private static ?mysqli $mysqli = null;

    // Using a Singleton
    public static function GetConnection(): mysqli {
        if(!is_object(self::$mysqli)) self::$mysqli = new mysqli(self::$host, self::$user, self::$pass, self::$db, self::$port);

        return self::$mysqli;
    }

    private function __destruct() {
        if(self::$mysqli) self::$mysqli->close();
    }

    private function __construct() {}
    private function __clone() {}


    public static function createSchemas() {
        if($result = self::GetConnection()->query("SHOW TABLES")) {
            $schemaArr = array();
            while($row = $result->fetch_assoc()) {
                array_push($schemaArr, $row["Tables_in_" . self::$db]);
            }

            if(!in_array("users", $schemaArr)) self::createUserSchema();
            if(!in_array("categories", $schemaArr)) self::createCategorySchema();
            if(!in_array("posts", $schemaArr)) self::createPostSchema();
            if(!in_array("comments", $schemaArr)) self::createCommentSchema();
        }
    }

    private static function createUserSchema() {
        // Got this from: https://code.tutsplus.com/tutorials/how-to-create-a-phpmysql-powered-forum-from-scratch--net-10188
        // I wanted to see how others would structure the database so I looked into it
        // I liked this person's implementation so I modified it slightly
        // DISCLAIMER: I am not using that website to create the whole forum,
        //             just wanted to see the database structure
        self::GetConnection()->query(
<<<'SQL'
CREATE TABLE users (
    user_id INT(8) NOT NULL AUTO_INCREMENT,
    user_name VARCHAR(30) NOT NULL,
    user_password VARCHAR(255) NOT NULL,
    user_privelege INT(8) NOT NULL,
    UNIQUE INDEX user_name_unique (user_name),
    PRIMARY KEY (user_id)
);
SQL
        );
    }

    private static function createCategorySchema() {
        // This is called a Nowdoc
        self::GetConnection()->query(
<<<'SQL'
CREATE TABLE categories (
    category_id INT(8) NOT NULL AUTO_INCREMENT,
    category_name VARCHAR(255) NOT NULL,
    category_description VARCHAR(255) NOT NULL,
    UNIQUE INDEX category_name_unique (category_name),
    PRIMARY KEY (category_id)
);
SQL
        );
    }

    private static function createPostSchema() {
        self::GetConnection()->query(
<<<'SQL'
CREATE TABLE posts (
    post_id INT(8) NOT NULL AUTO_INCREMENT,
    post_category_id INT(8) NOT NULL,
    post_user_id INT(8) NOT NULL,
    post_title VARCHAR(255) NOT NULL,
    post_created TIMESTAMP DEFAULT NOW(),
    UNIQUE INDEX post_name_unique (post_id),
    PRIMARY KEY (post_id)
);
SQL
        );
    }

    private static function createCommentSchema() {
        self::GetConnection()->query(
<<<'SQL'
CREATE TABLE comments (
    comment_id INT(8) NOT NULL AUTO_INCREMENT,
    comment_post_id INT(8) NOT NULL,
    comment_user_id INT(8) NOT NULL,
    comment_content TEXT NOT NULL,
    comment_created TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY (comment_id)
);
SQL
        );
    }
}

?>
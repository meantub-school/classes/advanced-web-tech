<?php

interface IEntity {
    // Returns true if the entity is saved
    public function save(): bool;

    /** returns an IEntity maybe if the selected entity exists in the Database */
    public static function getById($id);

    /** @return bool if the selected entity is edited successfully */
    public static function editById($id, $entityProperties): bool;

    /** @return bool if the selected entity is deleted successfully */
    public static function deleteById($id): bool;
}

?>
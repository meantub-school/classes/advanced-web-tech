<?php

Required::from("/api/db/Database.php");
Required::from("/api/db/IEntity.php");

class Comment implements IEntity {
    const AllowedEditProperties = array("comment_content");

    private int $comment_id;
    private int $comment_post_id;
    private int $comment_user_id;
    private string $comment_content;
    private string $comment_created;

    public function __construct($comment_post_id, $comment_user_id, $comment_content, $comment_id = -1, $comment_created = "") {
        $this->comment_id = $comment_id;
        $this->comment_post_id = $comment_post_id;
        $this->comment_user_id = $comment_user_id;
        $this->comment_content = $comment_content;
        $this->comment_created = $comment_created;
    }

    public function save(): bool {
        if($this->comment_post_id === null) return false;
        if($this->comment_user_id === null) return false;
        if($this->comment_content === null) return false;

        if(!Post::getById($this->comment_post_id)) return false;

        $commentAddedQuery = <<<SQL
            INSERT INTO comments (comment_post_id, comment_user_id, comment_content)
            VALUES ({$this->comment_post_id}, {$this->comment_user_id}, "{$this->comment_content}");
        SQL;

        $commentAdded = Database::GetConnection()->query($commentAddedQuery);
        if(!$commentAdded) return false;

        return true;

    }

    public static function getById($id) {
        $getCommentById = <<<SQL
            SELECT * FROM comments WHERE comment_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($getCommentById);
        if(!$resultOfQuery) return null;

        $commentDetails = $resultOfQuery->fetch_assoc();

        return new Comment(
            $commentDetails['comment_post_id'],
            $commentDetails['comment_user_id'],
            $commentDetails['comment_content'],
            $commentDetails['comment_id'],
            $commentDetails['comment_created']
        );
    }

    public static function editById($id, $entityProperties): bool {
        // Entity properties will be an associative array with properties
        // that you want to change the entity with so we need to transform it
        // into an array of strings

        $destructeredEntityProperties = array_map(function($key, $value) {
            if(in_array($key, self::AllowedEditProperties)) {
                if($key === "user_password") {
                    // In this case this is the only thing allowed to be
                    // Edited but we need to actually manipulate the password
                    // specifically before it goes into the DB
                    $escapedPassword = Database::GetConnection()->escape_string(password_hash($value, PASSWORD_DEFAULT));
                    return "$key='$escapedPassword'";

                } else {
                    $escapedValue = Database::GetConnection()->escape_string($value);
                    return "$key={$escapedValue}";
                }
            }
        }, array_keys($entityProperties), $entityProperties);
        if(sizeof($destructeredEntityProperties) === 0) return false;

        $removeEmptyProperties = array_filter($destructeredEntityProperties, fn($property) => strlen($property) !== 0);

        $reducedProperties = implode(",", $removeEmptyProperties);

        $editCommentByIdQuery = <<<SQL
            UPDATE comments SET {$reducedProperties} WHERE comment_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($editCommentByIdQuery);
        if(!$resultOfQuery) return false;

        return true;
    }

    public static function deleteById($id): bool {
        $deleteCommentById = <<<SQL
            DELETE FROM comments WHERE comment_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($deleteCommentById);
        if(!$resultOfQuery) return false;

        return true;
    }

    public static function getAllCommentsWithPostId($id) {
        $getAllCommentsWithPostIdQuery = <<<SQL
            SELECT *
            FROM comments c
            INNER JOIN (SELECT user_id, user_name, user_privelege FROM users) u
            ON c.comment_user_id=u.user_id
            WHERE c.comment_post_id={$id};
        SQL;

        $result = Database::GetConnection()->query($getAllCommentsWithPostIdQuery);
        if(!$result) return false;

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getId() {
        return $this->comment_id;
    }

    public function getPostId() {
        return $this->comment_post_id;
    }

    public function getUserId() {
        return $this->comment_user_id;
    }

    public function getContent() {
        return $this->comment_content;
    }

    public function getCreatedDate() {
        return $this->comment_created;
    }

}


?>
<?php

Required::from("/api/db/Database.php");
Required::from("/api/db/IEntity.php");

class Category implements IEntity {
    const AllowedEditProperties = array("category_name", "category_description");

    private int $category_id;
    private string $category_name;
    private string $category_description;

    public function __construct($category_name, $category_description, $category_id = -1) {
        $this->category_id = $category_id;
        $this->category_name = $category_name;
        $this->category_description = $category_description;
    }

    public function save(): bool {
        if($this->category_name === null) return false;
        if($this->category_description === null) return false;

        $categoryAddedQuery = <<<SQL
            INSERT INTO categories (category_name, category_description)
            VALUES ("{$this->category_name}", "{$this->category_description}");
        SQL;

        $result = Database::GetConnection()->query($categoryAddedQuery);
        if(!$result) return false;

        return true;
    }

    public static function getById($id) {
        $getCategoryByIdQuery = <<<SQL
            SELECT * FROM categories WHERE category_id={$id};
        SQL;

        $result = Database::GetConnection()->query($getCategoryByIdQuery);
        if($result->num_rows === 0) return null;

        $categoryDetails = $result->fetch_assoc();

        return new Category($categoryDetails['category_name'], $categoryDetails['category_description'], $categoryDetails['category_id']);
    }

    public static function editById($id, $entityProperties): bool {
        $destructeredEntityProperties = array_map(function($key, $value) {
            if(in_array($key, self::AllowedEditProperties)) {
                $escapedValue = Database::GetConnection()->escape_string($value);
                return "$key='{$escapedValue}'";
            }
        }, array_keys($entityProperties), $entityProperties);
        if(sizeof($destructeredEntityProperties) === 0) return false;

        $removeEmptyProperties = array_filter($destructeredEntityProperties, fn($property) => strlen($property) !== 0);

        $reducedProperties = implode(",", $removeEmptyProperties);

        $editCategoryByIdQuery = <<<SQL
            UPDATE categories SET {$reducedProperties} WHERE category_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($editCategoryByIdQuery);
        if(!$resultOfQuery) return false;

        return true;
    }

    public static function deleteById($id): bool {
        $deleteCategoryByIdQuery = <<<SQL
            DELETE FROM categories WHERE category_id={$id};
        SQL;

        $resultOfQuery = Database::GetConnection()->query($deleteCategoryByIdQuery);
        if(!$resultOfQuery) return false;

        return true;
    }

    public static function getAllCategories() {
        $getAllCategoriesQuery = <<<SQL
            SELECT * FROM categories;
        SQL;

        $resultOfQuery = Database::GetConnection()->query($getAllCategoriesQuery);
        if(!$resultOfQuery) return false;

        return $resultOfQuery->fetch_all(MYSQLI_ASSOC);
    }

    public function getName() {
        return $this->category_name;
    }

    public function getDescription() {
        return $this->category_description;
    }

    public function getId() {
        return $this->category_id;
    }
}

?>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/util/Required.php";
Required::from("/api/util/Router.php");
Required::from("/api/util/Context.php");
Required::from("/api/db/User.php");
Required::from("/api/db/Post.php");
Required::from("/api/db/Category.php");
Required::from("/api/db/Comment.php");

/**
 * The way that this works is that I have changed the .htaccess file
 * to Fallback only to this current file, it then creates a Router
 * object that I made that holds all the routes that are created.
 * The routes allow me to define everything manually as opposed to
 * using files to do routing. I think some MVC frameworks do this but
 * I modelled it off of Express.js that runs on the Node.js platform
 * Then at the end of the file it takes and processes the entire path,
 * gets the query params and puts them into the Context object which is
 * passed to each of the routes. The $router->match() function takes care of each incoming
 * request and runs the appropriate function supplied. That function will then handle
 * the business logic, such as accessing the Database.
 */


// Set up routes here
$router = new Router();

$router->get("/api/", function(Context $ctx) {
    $ctx->send(200, "API Working");
});

$router->get("/api/me", function(Context $ctx) {
    $ctx->send(200, ["success" => $ctx->getAuthenticationData()]);
});

$router->post("/api/post", function(Context $ctx) {
    $user = User::getById($ctx->getAuthenticationData()['user_id']);

    $ctx->CRUD(function (Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, ["error" => "Not authenticated"]);
            return;
        }

        ["category_id" => $category_id, "post_title" => $post_title] = $ctx->body()['body'];

        $post = new Post($category_id, $user->getId(), $post_title);

        if($postIsSaved = $post->save()) {
            $ctx->send(200, ["success" => $postIsSaved]);
        } else {
            $ctx->error(400, ["error" => $postIsSaved]);
        }
    },
    function(Context $ctx) {
        $post = Post::getById($ctx->body()['body']['post_id']);
        if(!$post) {
            $ctx->error(400, ["error" => "Post does not exist"]);
            return;
        }

        $ctx->send(200, [
            "success" => [
                "post_id" => $post->getId(),
                "category_id" => $post->getCategoryId(),
                "post_title" => $post->getTitle(),
                "user_id" => $post->getUserId()
            ]
        ]);
    },
    function(Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, ["error" => "Not authenticated"]);
            return;
        }

        if(Post::getById($ctx->body()['body']['post_id'])->getUserId() !== $user->getId()) {
            $ctx->error(400, ["error" => "You do not have permission to update other user's posts"]);
        }

        $postIsEdited = Post::editById($ctx->body()['body']['post_id'], $ctx->body()['body']);
        if(!$postIsEdited) {
            $ctx->error(400, ["error" => "Post editing failed"]);
            return;
        }

        $ctx->send(200, array("success" => $postIsEdited));
    },
    function(Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, array("error" => "Not authenticated"));
            return;
        }

        if(Post::getById($ctx->body()['body']['post_id'])->getUserId() !== $user->getId()) {
            $ctx->error(400, array("error" => "You do not have permission to update other user's posts"));
            return;
        }

        $postIsDeleted = Post::deleteById($ctx->body()['body']['post_id']);
        if(!$postIsDeleted) {
            $ctx->error(400, array("error" => "Post deletion failed"));
            return;
        }

        $ctx->send(200, array("success" => $postIsDeleted));
    });
});

$router->post("/api/comment", function(Context $ctx) {
    $user = User::getById($ctx->getAuthenticationData()['user_id']);

    $ctx->CRUD(function (Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, array("error" => "Not authenticated"));
            return;
        }

        [
            "comment_post_id" => $comment_post_id,
            "comment_content" => $comment_content
        ] = $ctx->body()['body'];

        $comment = new Comment($comment_post_id, $user->getId(), $comment_content);

        if($commentIsSaved = $comment->save()) {
            $ctx->send(200, ["success" => $commentIsSaved]);
        } else {
            $ctx->error(400, ["error" => $commentIsSaved]);
        }
    },
    function(Context $ctx) {
        $comment = Comment::getById($ctx->body()['body']['comment_id']);
        if(!$comment) {
            $ctx->error(400, ["error" => "Comment does not exist"]);
            return;
        }

        $ctx->send(200, [
            "success" => [
                "comment_id" => $comment->getId(),
                "comment_post_id" => $comment->getPostId(),
                "comment_user_id" => $comment->getUserId(),
                "comment_content" => $comment->getContent(),
                "comment_created" => $comment->getCreatedDate()
            ]
        ]);
    },
    function(Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, array("error" => "Not authenticated"));
            return;
        }

        if(Comment::getById($ctx->body()['body']['comment_id'])->getUserId() !== $user->getId()) {
            $ctx->error(400, array("error" => "You do not have permission to update other user's comments"));
            return;
        }

        $commentIsEdited = Comment::editById($ctx->body()['body']['comment_id'], $ctx->body()['body']);
        if(!$commentIsEdited) {
            $ctx->error(400, array("error" => "Comment editing failed"));
            return;
        }

        $ctx->send(200, array("success" => $commentIsEdited));
    },
    function(Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, array("error" => "Not authenticated"));
            return;
        }

        if(Comment::getById($ctx->body()['body']['comment_id'])->getUserId() !== $user->getId()) {
            $ctx->error(400, array("error" => "You do not have permission to delete other user's comments"));
            return;
        }

        $commentIsDeleted = Comment::deleteById($ctx->body()['body']['comment_id']);
        if(!$commentIsDeleted) {
            $ctx->error(400, array("error" => "Comment deletion failed"));
            return;
        }

        $ctx->send(200, array("success" => $commentIsDeleted));
    });
});

$router->post("/api/category", function(Context $ctx) {
    $user = User::getById($ctx->getAuthenticationData()['user_id']);

    $ctx->CRUD(function (Context $ctx) use($user) {
        if($user === null || !$user->isAdmin()) {
            $ctx->error(400, array("error" => "Need admin priveleges"));
            return;
        }
        ["category_name" => $category_name, "category_description" => $category_description] = $ctx->body()['body'];

        $category = new Category($category_name, $category_description);

        if($categoryIsSaved = $category->save()) {
            $ctx->send(200, array("success" => $categoryIsSaved));
        } else {
            $ctx->error(400, array("error" => $categoryIsSaved));
        }
    },
    function(Context $ctx) {
        $categoryById = Category::getById($ctx->body()['body']['category_id']);
        if(!$categoryById) {
            $ctx->error(400, array("error" => "Category does not exist"));
            return;
        }

        $ctx->send(200, array("success" => array(
            "category_id" => $categoryById->getId(),
            "category_name" => $categoryById->getName(),
            "category_description" => $categoryById->getDescription()
        )));
    },
    function(Context $ctx) use($user) {
        if($user === null || !$user->isAdmin()) {
            $ctx->error(400, array("error" => "Need admin priveleges"));
            return;
        }

        $categoryIsEdited = Category::editById($ctx->body()['body']['category_id'], $ctx->body()['body']);
        if(!$categoryIsEdited) {
            $ctx->error(400, array("error" => "Category editing failed"));
            return;
        }

        $ctx->send(200, array("success" => $categoryIsEdited));
    },
    function(Context $ctx) use($user) {
        if($user === null || !$user->isAdmin()) {
            $ctx->error(400, array("error" => "Need admin priveleges"));
            return;
        }

        $categoryIsDeleted = Category::deleteById($ctx->body()['body']['category_id']);
        if(!$categoryIsDeleted) {
            $ctx->error(400, array("error" => "Category deletion failed"));
            return;
        }

        $ctx->send(200, array("success" => $categoryIsDeleted));
    });
});

$router->get("/api/categories", function(Context $ctx) {
    $categories = Category::getAllCategories();

    $ctx->send(200, array("success" => $categories));
});

$router->post("/api/posts", function(Context $ctx) {
    ["category_id" => $category_id] = $ctx->body();
    $posts = Post::getAllPostsWithCategoryId($category_id);

    $ctx->send(200, array("success" => $posts));
});

$router->post("/api/comments", function(Context $ctx) {
    ["post_id" => $post_id] = $ctx->body();
    $comments = Comment::getAllCommentsWithPostId($post_id);

    $ctx->send(200, array("success" => $comments));
});

$router->post("/api/user", function(Context $ctx) {
    $user = User::getById($ctx->getAuthenticationData()['user_id']);

    $ctx->CRUD(function (Context $ctx) {
        $ctx->error(400, array("error" => "Create is not supported"));
    },
    function(Context $ctx) {
        $userById = User::getById($ctx->body()["body"]["user_id"]);
        if(!$userById) {
            $ctx->error(400, array("error" => "user does not exist"));
            return;
        }

        $ctx->send(200, array("success" => array(
            "user_name" => $userById->getUserName(),
            "user_privelege" => EPrivelege::toString($userById->getPrivelege())
        )));
    },
    function(Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, array("error" => "Not authenticated"));
            return;
        }

        $userIsEdited = User::editById($user->getId(), $ctx->body()['body']);
        if(!$userIsEdited) {
            $ctx->error(400, array("error" => "User editing failed"));
            return;
        }

        $ctx->send(200, array("success" => $userIsEdited));
    },
    function(Context $ctx) use($user) {
        if(!$user) {
            $ctx->error(400, array("error" => "Not authenticated"));
            return;
        }

        $userIsDeleted = User::deleteById($user->getId());
        if(!$userIsDeleted) {
            $ctx->error(400, array("error" => "User deletion failed"));
            return;
        }

        $ctx->send(200, array("success" => $userIsDeleted));
    });
});

$router->post("/api/signup", function(Context $ctx) {
    // Array destructuring, look it up it's something that is in JS
    // So I figured PHP must have it too, some nice syntactic sugar
    ["username" => $username, "password" => $password] = $ctx->body();
    $user = new User($username, $password, 0);

    if($user->save()) {
        $ctx->send(200, array("success" => "user signed up"));
    }
    else {
        $ctx->error(400, array("failed" => "user failed to be signed up"));
    }
});

$router->post("/api/login", function(Context $ctx) {
    ["username" => $username, "password" => $password] = $ctx->body();
    $user = new User($username, $password);

    if($user->exists()) {
        $ctx->send(200, array("success" => $user->generateToken()));
    }
    else {
        $ctx->error(400, array("failed" => "user failed to be logged in"));
    }
});

// Set up database if it is not set up
Database::createSchemas();

// Do this for JSON body in POST requests
$body = file_get_contents('php://input');

$jsonBody = json_decode($body, true);

// Do this for Query Params in GET requests
$parsedPath = Router::parse_path();

$parsedParams = Router::parse_query_params($parsedPath['query']);

// Use this to get authorization headers
$authHeader = getallheaders()['Authorization'] !== null ? getallheaders()['Authorization'] : null;

$router->match($_SERVER['REQUEST_METHOD'], $parsedPath['path'], new Context($parsedParams, $jsonBody, $authHeader));

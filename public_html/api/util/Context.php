<?php

class Context
{
    private $queryParams = null;
    private $body = null;
    private $authorizationData = null;

    public function __construct($queryParams = array(), $body = array(), $authorizationHeader = null) {
        $this->queryParams = $queryParams;
        $this->body = $body;

        if ($decodedPayload = User::decodeToken($authorizationHeader)) {
            $this->authorizationData = $decodedPayload;
        } else {
            $this->authorizationData = null;
        }
    }

    public function queryParams()
    {
        return $this->queryParams;
    }

    public function body()
    {
        return $this->body;
    }

    public function isAuth()
    {
        return $this->authorizationData !== null;
    }

    public function getAuthenticationData()
    {
        return $this->authorizationData !== null ? $this->authorizationData : false;
    }

    public function error($errorCode, $responseBody): void
    {
        header('Content-Type: application/json');
        http_response_code($errorCode);
        echo json_encode(array('code' => $errorCode, 'error' => $responseBody));
    }

    public function send($statusCode, $responseBody): void
    {
        header('Content-Type: application/json');
        echo json_encode(array('code' => $statusCode, 'data' => $responseBody));
    }


    public function CRUD(callable $createCallback, callable $readCallback, callable $updateCallback, callable $deleteCallback) {
        if ($this->body === null) return;

        switch ($this->body['method']) {
            case "CREATE":
                $createCallback($this);
                break;
            case "READ":
                $readCallback($this);
                break;
            case "UPDATE":
                $updateCallback($this);
                break;
            case "DELETE":
                $deleteCallback($this);
                break;
            default:
                $this->error(400, array("error" => "Incorrect usage of api"));
                break;
        }
    }

}
?>
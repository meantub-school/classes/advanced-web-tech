<?php

class Router {
    private array $routes = array();

    public function __construct() {}

    /**
     * @param $route is the path (Ex: "/user/1")
     * @param $routeFunction is the function that should be called if it
     */
    public function get(string $path, callable $routeFunction) {
        array_push($this->routes, array(
            'method' => "GET",
            'path' => $path,
            'routeFunction' => $routeFunction
        ));
    }

    public function post(string $path, callable $routeFunction) {
        array_push($this->routes, array(
            'method' => "POST",
            'path' => $path,
            'routeFunction' => $routeFunction
        ));
    }

    public function match(string $method, string $path, Context $context) {
        $matchedRoutes = array_filter($this->routes, function($route) use ($method, $path) {
            // If the path contains :{something} then we have to do fancy matching somehow
            return $route['method'] === $method && $route['path'] === $path;
        });

        if(count($matchedRoutes) === 0) {
            $context->error(404, "Nothing found");
        }

        foreach($matchedRoutes as $matchedRoute) {
            $matchedRoute['routeFunction']($context);
        }
    }

    public static function parse_path() {
        $url = ($_SERVER['HTTPS'] == "on" ? "https://" : "http://");

        if ($_SERVER["SERVER_PORT"] != "80") {
            $url .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $url .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return parse_url($url);
    }

    public static function parse_query_params($paramsString) {
        if($paramsString === null) return null;

        $splitStrings = preg_split("/[\&]/", $paramsString);

        $paramArray = array();

        foreach($splitStrings as $paramString) {
            [$key, $value] = preg_split("/[\=]/", $paramString);
            $paramArray[$key] = $value;
        }

        return $paramArray;
    }

}

?>
// Ken's (not really) React since i can't use regular React
import { App } from "./index.js";

let states = [];
let stateCursor = 0;

export const resetStates = () => {
  states = [];
};

export const render = (kreactElement, container) => {
  if (typeof kreactElement === "undefined" || typeof kreactElement === "null") {
    return;
  }

  if (typeof kreactElement === "string" || typeof kreactElement === "number") {
    container.appendChild(document.createTextNode(String(kreactElement)));
    return;
  }

  const actualDomElement = document.createElement(kreactElement.tag);

  if (kreactElement.props) {
    Object.keys(kreactElement.props)
      .filter((p) => p !== "children")
      .forEach((p) => (actualDomElement[p] = kreactElement.props[p]));
  }

  if (kreactElement.children) {
    kreactElement.children.forEach((child) => render(child, actualDomElement));
  }

  container.appendChild(actualDomElement);
};

export const rerender = () => {
  stateCursor = 0;
  if (document.getElementById("root").firstChild !== null) {
    document.getElementById("root").firstChild.remove();
  }
  render(App(), document.getElementById("root"));
};

export const createElement = (tag, props, ...children) => {
  if (typeof tag == "function") {
    return tag(props);
  }
  const element = { tag, props: { ...props }, children };

  return element;
};

export const useState = (initialState) => {
  const FROZEN_CURSOR = stateCursor;
  states[FROZEN_CURSOR] = states[FROZEN_CURSOR] || initialState;

  const state = () => states[FROZEN_CURSOR];

  const setState = (newState) => {
    states[FROZEN_CURSOR] = newState;
    rerender();
  };

  stateCursor++;

  return [state(), setState];
};

export const useEffect = (callback, depArray) => {
  const FROZEN_CURSOR = stateCursor;
  const hasChanged = depArray !== states[FROZEN_CURSOR];
  if (depArray === undefined || hasChanged) {
    callback();
    states[FROZEN_CURSOR] = depArray;
  }

  stateCursor++;
  return () => console.log("unsubscribed effect");
};

import { createElement as h, rerender, resetStates } from "./kreact.js";

export const useHistory = () => {
  const push = (link) => {
    history.pushState({}, "", link);
  };

  return { push };
};

export const Link = (route, children) =>
  h("b", { onclick: (event) => history.pushState({}, "", route) }, children);

export const Route = (route, component) => {
  const variableNames = [];

  const extractedVariableRoute = route.replace(
    /(?:[:*])(\w+)/g,
    (full, name) => {
      variableNames.push(name);
      return "([^\\/]+)";
    }
  );

  const endOfRoute = "(?:\\/|$)";

  const regex = new RegExp(extractedVariableRoute + endOfRoute);

  const matchRoute = (url) => {
    if (component === undefined || component === null)
      return { isMatch: false, params: {}, component: {} };

    const possibleMatch = url.match(regex);
    if (possibleMatch === null)
      return { isMatch: false, params: {}, component: {} };
    if (possibleMatch.length === 1)
      return { isMatch: true, params: {}, component: component };

    // Gets rid of first return value from regex match
    const params = possibleMatch.slice(1, possibleMatch.length);

    const paramObject = Object.assign(
      ...variableNames.map((key, index) => ({ [key]: params[index] }))
    );

    return { isMatch: true, params: paramObject, component: component };
  };

  return matchRoute;
};

export const Switch = (url, fallbackRoute, ...routes) => {
  const routesThatMatch = routes
    .filter((route) => route(url).isMatch)
    .map((route, index) => {
      return route(url).component(route(url).params);
    });

  if (routesThatMatch.length === 0) {
    return h("div", { id: "switch" }, fallbackRoute);
  }

  return h("div", { id: "switch" }, ...routesThatMatch);
};

/// Some stuff to work with the history events

// By default there is no event when the browser
// changes it's history with history.pushState
// So I changed the function to emit an event
const _overwriteHistory = (type) => {
  const orig = history[type];

  // Need regular function here because arrow functions
  // are not constructable so you cannot get an
  // arguments array
  return function () {
    const rv = orig.apply(this, arguments);
    const event = new Event(type);
    event.arguments = arguments;
    window.dispatchEvent(event);
    return rv;
  };
};

history.pushState = _overwriteHistory("pushState");
history.replaceState = _overwriteHistory("replaceState");

window.addEventListener("pushState", (event) => {
  resetStates();
  rerender();
});

window.addEventListener("replaceState", (event) => {
  resetStates();
  rerender();
});

window.onpopstate = (event) => {
  resetStates();
  rerender();
};

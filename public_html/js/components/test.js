import { useState, useEffect, createElement as h } from "../kreact.js";
import { useHistory } from "../krouter.js";

const Test = (params) => {
  const [count, setCount] = useState(0);
  const history = useHistory();

  return h(
    "div",
    {},
    h("p", {}, `${count}`),
    h("button", { onclick: (event) => setCount(count + 1) }, `Increment`),
    h("button", { onclick: (event) => history.push("/login") }, `Wow`),
    h("div", {}, `${params && params.id}`)
  );
};

export default Test;

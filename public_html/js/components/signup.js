import { useState, useEffect, createElement as h } from "../kreact.js";
import { post } from "../kajax.js";

const Signup = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const submitForm = () => {
    if (username !== "" && password !== "") {
      post("/api/signup", JSON.stringify({ username, password })).then(
        (response) => {
          console.log(response);
        }
      );
    }
  };

  return h(
    "div",
    {},
    h("h1", {}, `Signup`),
    h("p", {}, `Username: `),
    h(
      "input",
      {
        type: "text",
        onchange: (event) => setUsername(event.target.value),
        value: username,
      },
      ""
    ),
    h("p", {}, `Password: `),
    h(
      "input",
      {
        type: "password",
        onchange: (event) => setPassword(event.target.value),
        value: password,
      },
      ""
    ),
    h("br", {}, ``),
    h("button", { onclick: (event) => submitForm() }, `Submit`)
  );
};

export default Signup;

import { useState, useEffect, createElement as h } from "../kreact.js";
import { post } from "../kajax.js";
import { useHistory } from "../krouter.js";

import Comment from "./comment.js";

const Post = (params) => {
  const [postDetails, setPostDetails] = useState({});
  const [comments, setComments] = useState([]);
  const [createCommentContent, setCreateCommentContent] = useState([]);
  const history = useHistory();

  useEffect(async () => {
    await post(
      "/api/post",
      JSON.stringify({
        method: "READ",
        body: { post_id: params.id },
      })
    )
      .then((res) => JSON.parse(res))
      .then((res) => setPostDetails(res.data.success));

    await post(
      "/api/comments",
      JSON.stringify({
        post_id: params.id,
      })
    )
      .then((res) => JSON.parse(res))
      .then((res) => setComments(res.data.success));
  }, null);

  const commentsRender = h(
    "div",
    {},
    ...comments.map(
      ({
        comment_created,
        comment_content,
        comment_user_id,
        user_name,
        user_privelege,
      }) =>
        Comment(comment_created, comment_content, {
          userId: comment_user_id,
          userName: user_name,
          userPrivelege: user_privelege,
        })
    )
  );

  const submitCreateComment = () => {
    if (createCommentContent == "") return;

    post(
      "/api/comment",
      JSON.stringify({
        method: "CREATE",
        body: {
          comment_post_id: params.id,
          comment_content: createCommentContent,
        },
      }),
      {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      }
    )
      .then((res) => JSON.parse(res))
      .then((res) => {
        if (res.data.success) {
          history.push(`/post/${params.id}`);
        }
      })
      .catch((err) => {
        alert(`You're not authenticated\n${err}`);
      });
  };

  const createCommentRender = h(
    "div",
    { className: "form-group" },
    h("textarea", {
      className: "form-control",
      onchange: (event) => setCreateCommentContent(event.target.value),
      value: createCommentContent,
      rows: "3",
    }),
    h(
      "button",
      {
        className: "btn btn-primary my-2",
        onclick: (event) => submitCreateComment(),
      },
      `Create Comment`
    )
  );

  return h(
    "div",
    {},
    h("h1", {}, postDetails?.post_title),
    commentsRender,
    createCommentRender
  );
};

export default Post;

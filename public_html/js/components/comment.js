import { createElement as h } from "../kreact.js";

const Comment = (
  commentCreated,
  commentContent,
  { userId, userName, userPrivelege }
) => {
  return h(
    "div",
    { className: "card mb-3" },
    h(
      "div",
      { className: "row no-gutters" },
      h(
        "div",
        { className: "col-md-8" },
        h(
          "div",
          { className: "card-body" },
          h("h5", { className: "card-title" }, userName),
          h("p", { className: "card-text" }, commentContent),
          h(
            "p",
            { className: "card-text" },
            h("small", { className: "text-muted" }, commentCreated)
          )
        )
      )
    )
  );
};

export default Comment;

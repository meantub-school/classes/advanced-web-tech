import { useState, useEffect, createElement as h } from "../kreact.js";
import { get } from "../kajax.js";
import { useHistory } from "../krouter.js";

const Categories = () => {
  const history = useHistory();
  const [categories, setCategories] = useState([]);

  useEffect(async () => {
    await get("/api/categories")
      .then((res) => JSON.parse(res))
      .then((res) => setCategories(res.data.success));
  }, null);

  const categoryRender = categories.map((category) =>
    h(
      "a",
      {
        href: "#",
        className: "list-group-item list-group-item-action",
        onclick: () => history.push(`/category/${category.category_id}`),
      },
      h(
        "div",
        { className: "d-flex w-100 justify-content-between" },
        h("h5", { className: "mb-1" }, category.category_name)
      ),
      h("p", { className: "mb-1" }, category.category_description)
    )
  );

  return h(
    "div",
    {},
    h("h1", {}, `Categories`),
    h("list-group", {}, ...categoryRender)
  );
};

export default Categories;

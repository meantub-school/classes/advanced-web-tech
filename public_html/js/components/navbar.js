import { useState, useEffect, createElement as h } from "../kreact.js";
import { useHistory } from "../krouter.js";

const Navbar = () => {
  const history = useHistory();

  const isSignedIn = localStorage.getItem("token") !== null;

  return h(
    "nav",
    { className: "navbar navbar-dark bg-dark" },
    h(
      "a",
      {
        href: "#",
        className: "navbar-brand",
        onclick: (e) => history.push("/"),
      },
      `Forum`
    ),
    h(
      "ul",
      { className: "navbar-nav mr-auto" },
      h(
        "li",
        { className: "nav-item" },
        h(
          "a",
          {
            href: "#",
            className: "nav-link",
            onclick: (e) => history.push("/categories"),
          },
          `Categories`
        )
      )
    ),
    !isSignedIn
      ? h(
          "form",
          { className: "form-inline" },
          h(
            "div",
            { className: "input-group" },
            h(
              "div",
              { className: "input-group-prepend" },
              h(
                "button",
                {
                  className: "btn btn-outline-success my-2 my-sm-0",
                  onclick: (event) => history.push("/login"),
                },
                `Login`
              )
            ),
            h(
              "div",
              {
                className: "btn btn-success",
                onclick: (event) => history.push("/signup"),
              },
              `Signup`
            )
          )
        )
      : h(
          "button",
          {
            className: "btn btn-warning",
            onclick: (event) => {
              localStorage.removeItem("token");
              history.push("/");
            },
          },
          `Logout`
        )
  );
};

export default Navbar;

import { createElement as h } from "../kreact.js";

const NotFound = () => h("div", {}, `Not Found`);

export default NotFound;

import { useState, useEffect, createElement as h } from "../kreact.js";
import { post } from "../kajax.js";
import { useHistory } from "../krouter.js";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const submitForm = () => {
    if (username !== "" && password !== "") {
      post("/api/login", JSON.stringify({ username, password }))
        .then((res) => JSON.parse(res))
        .then((response) => {
          if (response.data) {
            localStorage.setItem("token", response.data.success);
            history.push("/");
          } else {
            console.log(response);
          }
        });
    }
  };

  return h(
    "div",
    {},
    h("h1", {}, `Login`),
    h("p", {}, `Username: `),
    h(
      "input",
      {
        type: "text",
        onchange: (event) => setUsername(event.target.value),
        value: username,
      },
      ""
    ),
    h("p", {}, `Password: `),
    h(
      "input",
      {
        type: "password",
        onchange: (event) => setPassword(event.target.value),
        value: password,
      },
      ""
    ),
    h("br", {}, ``),
    h("button", { onclick: (event) => submitForm() }, `Submit`)
  );
};

export default Login;

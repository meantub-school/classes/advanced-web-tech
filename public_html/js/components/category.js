import { useState, useEffect, createElement as h } from "../kreact.js";
import { post } from "../kajax.js";
import { useHistory } from "../krouter.js";

const Category = (params) => {
  const [category, setCategory] = useState({});
  const [posts, setPosts] = useState([]);
  const [createPostTitle, setCreatePostTitle] = useState("");

  const history = useHistory();

  useEffect(async () => {
    await post(
      "/api/category",
      JSON.stringify({
        method: "READ",
        body: { category_id: params.id },
      })
    )
      .then((res) => JSON.parse(res))
      .then((res) => setCategory(res.data.success));

    await post(
      "/api/posts",
      JSON.stringify({
        category_id: params.id,
      })
    )
      .then((res) => JSON.parse(res))
      .then((res) => setPosts(res.data.success));
  }, null);

  const postsRender = h(
    "list-group",
    {},
    ...posts
      .sort((a, b) => new Date(b.post_created) - new Date(a.post_created))
      .map((post) =>
        h(
          "a",
          {
            href: "#",
            className: "list-group-item list-group-item-action",
            onclick: () => history.push(`/post/${post.post_id}`),
          },
          h(
            "div",
            { className: "d-flex w-100 justify-content-between" },
            h("h5", { className: "mb-1" }, post.post_title),
            h("small", {}, post.post_created)
          ),
          h("p", { className: "mb-1 text-muted" }, `by ${post.user_name}`)
        )
      )
  );

  const createPostSubmit = () => {
    if (createPostTitle == "") return;

    post(
      "/api/post",
      JSON.stringify({
        method: "CREATE",
        body: {
          category_id: params.id,
          post_title: createPostTitle,
        },
      }),
      {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      }
    )
      .then((res) => JSON.parse(res))
      .then((res) => {
        if (res.data.success) {
          history.push(`/category/${params.id}`);
        }
      })
      .catch((err) => {
        alert(`You're not authenticated\n${err}`);
      });
  };

  const createPostRender = h(
    "div",
    { className: "form-group my-2" },
    h("label", {}, `Post Title`),
    h("input", {
      type: "text",
      className: "form-control",
      onchange: (event) => setCreatePostTitle(event.target.value),
      value: createPostTitle,
    }),
    h(
      "button",
      {
        className: "btn btn-primary my-2",
        onclick: (event) => createPostSubmit(),
      },
      `Create Post`
    )
  );

  return h(
    "div",
    {},
    h("h1", {}, `${category.category_name}`),
    postsRender,
    createPostRender
  );
};

export default Category;

import {
  createElement as h,
  useState,
  useEffect,
  render,
  rerender,
} from "./kreact.js";
import { Route, Switch, Link } from "./krouter.js";

import Category from "./components/category.js";
import Login from "./components/login.js";
import Signup from "./components/signup.js";
import Navbar from "./components/navbar.js";
import Categories from "./components/categories.js";
import NotFound from "./components/notfound.js";
import Post from "./components/post.js";

export const App = () => {
  const returnValue = h(
    "div",
    { className: "container" },
    Navbar(),
    h(
      "div",
      { className: "bg-light", style: "padding: 30px", id: "page-container" },
      Switch(
        window.location.pathname,
        NotFound(),
        Route("/", (params) => Categories()),
        Route("/login", (params) => Login()),
        Route("/signup", (params) => Signup()),
        Route("/categories", (params) => Categories()),
        Route("/category/:id", (params) => Category(params)),
        Route("/post/:id", (params) => Post(params))
      )
    )
  );

  return returnValue;
};

render(App(), document.getElementById("root"));

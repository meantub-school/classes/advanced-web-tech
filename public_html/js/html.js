// My attempt a template literal that will parse it into a
// Kreact AST was more difficult that suspected

import * as Kreact from './kreact.js';


// Template literal
const html = (strings, ...keys) => {
  if(keys.length === 0) {
    return strings.raw;
  }

  let tempString = "";

  strings.raw.forEach((str, index) => {
    tempString += str + (keys[index] || '');
  });

  const parser = new DOMParser();
  const parsedHTML = parser.parseFromString(tempString, 'application/xml');

  console.log(parsedHTML);

  let dummyElement = document.createElement('div');
  dummyElement.innerHTML = tempString;
  // console.log(tempString);

  // console.log(dummyElement.children[0])
  // console.log(parseDOMtoKreact(dummyElement.children[0]));

  return parseDOMtoKreact(dummyElement.children[0]);
};

const parseDOMtoKreact = (element) => {

  if(element.nodeName === "#text") {
    if(!(/[^\t\n\r ]/.test(element.textContent))) {
      // console.log("all whitespace")
      // console.log(element);
      return "";
    }
    return String(element.textContent);
  }

  const props = Object.values(element.attributes).map(value => {
    return [value.name, value.nodeValue];
  });

  return Kreact.createElement(
    element.localName,
    { ...Object.fromEntries(props) },
    ...Array.from(element.childNodes).map(childElement =>
      parseDOMtoKreact(childElement)
    )
  );
}

export default html;

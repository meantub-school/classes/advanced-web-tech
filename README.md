# Requirements

The term-project must be dynamic Web 2.0 style with client and server side programming that contains ALL of the following components and technologies:
- HTML5
- CSS
- JavaScript on the client side
- MySQL Database
- PHP

Optional:
- AJAX
- XML
- JSON

Miscellaneous:
- The term-project is on individual basis, there are no groups.
- The scope of the project is to have solid 4-6 weeks development of the actual code.
- Also allow time for testing and debugging.
- A 4 credit hour course yield to spending minimum of 12 hours/week at home on the coursework.
- That figure can guide you to scope your project along with other coursework required for this class.
- A fully working project by the first demo & presentation day is required to earn full credit.
- Term-projects that are not fully functioning, have missing features, bugs, etc. will be graded accordingly.

Notes:
- No Framework allowed on the server side unless you have my written authorization.
- CSS and JavaScript framework and/or libraries on the client side is acceptable.
- The server side code must be in PHP.

# YouTube Video Explaining Project
https://youtu.be/Lm4GZFRigD4
openapi: 3.0.0
info:
  version: 1.0.0
  title: Advanced Web Tech
servers:
  - url: 'http://localhost:3000'
paths:
# Posts
  /post:
    post:
      description: 'Creates a post, returns post details'
      parameters:
        - name: title
          in: query
          description: Title of post
          required: true
          schema:
            type: string
        - name: categoryId
          in: query
          description: Category that you want the post to be in
          required: true
          schema:
            type: integer
            format: int32
      responses:
        '200':
          description: 'Details about the post'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Post'

  /post/{postId}:
    get:
      description: 'Returns a list of comments in a post'
      responses:
        '200':
          description: 'Details about a post'

    put:
      description: 'Updates a post'
      responses:
        '200':
          description: OK

    delete:
      description: 'Removes the post from the DB'
      responses:
        '200':
          description: OK

# Catgories

  /categories:
    get:
      description: 'Gets all categories'
      responses:
        '200':
          description: OK

  /category:
    post:
      description: 'Creates a category, returns category'
      responses:
        '200':
          description: OK

  /category/{categoryId}:
    get:
      description: 'Returns a list of post ids in a category'
      responses:
        '200':
          description: OK

    put:
      description: 'Updates a category'
      responses:
        '200':
          description: OK

    delete:
      description: 'Removes the category from the DB'
      responses:
        '200':
          description: OK

# Comments
  /comment:
    post:
      description: 'Creates a comment, returns comment ID'
      responses:
        '200':
          description: OK

  /comment/{commentId}:
    get:
      description: 'Returns comment details'
      responses:
        '200':
          description: OK

    put:
      description: 'Updates a comment'
      responses:
        '200':
          description: OK

    delete:
      description: 'Removes the comment from the DB'
      responses:
        '200':
          description: OK

  /user:
    get:
      summary: 'Returns details about a particular user'
      operationId: listUser
      tags:
        - user
      parameters:
        - name: id
          in: query
          description: ID of the user
          required: true
          schema:
            type: integer
            format: int32
      responses:
        '200':
          description: 'Details about a user by ID'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /signup:
    post:
      summary: 'Signup Endpoint: Creates a user that you can log in with'
      tags:
        - user
      parameters:
        - name: username
          in: query
          description: Username of the user
          required: true
          schema:
            type: string
        - name: password
          in: query
          description: Password of the user
          required: true
          schema:
            type: string
      responses:
        '200':
          description: User details
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /login:
    get:
      description: 'Gives user a token that they use for subsequent requests'
      parameters:
        - name: username
          in: query
          required: true
          schema:
            type: string
        - name: password
          in: query
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserToken'


components:
  schemas:
    User:
      type: object
      required:
        - id
        - name
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
        tag:
          type: string

    UserToken:
      type: object
      required:
        - token
      properties:
        token:
          type: string

    Error:
      type: object
      required:
        - code
        - message
      properties:
        code:
          type: integer
          format: int32
        message:
          type: string

    Category:
      type: object
      required:
        - id
        - name
        - description
      properties:
        id:
          type: integer
          format: int32
        name:
          type: string
        description:
          type: string

    Post:
      type: object
      required:
        - id
        - categoryId
        - userId
        - title
      properties:
        id:
          type: integer
          format: int32
        categoryId:
          type: integer
          format: int32
        title:
          type: string
        description:
          type: string
        userId:
          type: integer
          format: int32

    Comment:
      type: object
      required:
        - id
        - postId
        - userId
        - commentContent
      properties:
        id:
          type: integer
          format: int32
        postId:
          type: integer
          format: int32
        userId:
          type: integer
          format: int32
        commentContent:
          type: string
